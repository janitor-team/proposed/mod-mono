# Vietnamese translation for Mod-Mono.
# Copyright © 2007 Free Software Foundation, Inc.
# Clytie Siddall <clytie@riverland.net.au>, 2007
#
msgid ""
msgstr ""
"Project-Id-Version: mod-mono\n"
"Report-Msgid-Bugs-To: mod-mono@packages.debian.org\n"
"POT-Creation-Date: 2011-03-20 23:00+0000\n"
"PO-Revision-Date: 2007-07-26 23:31+0930\n"
"Last-Translator: Clytie Siddall <clytie@riverland.net.au>\n"
"Language-Team: Vietnamese <vi-VN@googlegroups.com>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: LocFactoryEditor 1.7.1b\n"

#. Type: select
#. Description
#: ../libapache2-mod-mono.templates:2001
msgid "Mono server to use:"
msgstr "Máy phục vụ Mono cần dùng:"

#. Type: select
#. Description
#: ../libapache2-mod-mono.templates:2001
#, fuzzy
#| msgid ""
#| "The libapache2-mod-mono module can be used with one of two different Mono "
#| "ASP.NET backends:\n"
#| " - mod-mono-server : implements ASP.NET 1.1 features;\n"
#| " - mod-mono-server2: implements ASP.NET 2.0 features."
msgid ""
"The libapache2-mod-mono module can be used with one of two different Mono "
"ASP.NET backends:\n"
" - mod-mono-server2: implements ASP.NET 2.0 features;\n"
" - mod-mono-server4: implements ASP.NET 4.0 features."
msgstr ""
"Mô-đun libapache2-mod-mono có thể chạy với một của hai hậu phương ASP.NET "
"Mono:\n"
" • mod-mono-server2\t\tthực hiện các tính năng ASP.NET 2.0;\n"
" • mod-mono-server4\t\tthực hiện các tính năng ASP.NET 4.0."
